import time


class Error(Exception):
    """A custom exception used to report errors in use of Microwave class"""


class Microwave():
    """A class simulating microwave functionality"""
    def __init__(self):
        self.cook_power = 5
        self.cook_program = 'cook'
        self.cook_time = '00:30'
        self.power_on = False
        self.turn_table = True
        self.started = False

    def power_button(self):
        """set power on/off"""
        if self.power_on == False:
            self.power_on = True
        else:
            self.power_on = True
        print('the microwave is %s now' % ('on' if self.power_on else 'off'))

    def check_power(self):
        """raise error if power is off"""
        if self.power_on == 0:
            raise Error(f'Microwave off, turn it on first')

    def set_power_level(self, power_level=None):
        """set cook power level in scale of 1 to 10"""
        self.check_power()
        if not power_level or power_level > 10:
            raise Error(f'please enter a power level in scale of 1 to 10')
        else:
            self.cook_power = power_level
            print('cook power set to %d' % self.cook_power)

    def set_turn_table(self, turn_status=None):
        """set the turn table on/off"""
        if not turn_status or turn_status not in ['on', 'off']:
            raise Error(f'please enter on/off')
        self.turn_table = (turn_status == 'on')

    def set_timer(self, time_str):
        """set the timer in format of MM:SS"""
        self.check_power()
        mins, secs = (int(x) for x in time_str.split(':'))
        timer_secs = mins * 60 + secs
        if timer_secs == 0:
            return
        print('%02d:%02d' % (mins, secs))
        for s in range(timer_secs):
            time.sleep(1)
            remain_mins = (timer_secs - s - 1) // 60
            remain_secs = (timer_secs - s - 1) % 60
            print('%02d:%02d' % (remain_mins, remain_secs))

    def set_program(self, cook_program):
        """set a predefined program such as defrost/popcorn"""
        self.check_power()
        self.cook_program = cook_program
        if self.cook_program == 'defrost':
            self.power_level = 2
            self.cook_time = '05:00'
            self.turn_table = True

        elif self.cook_program == 'popcorn':
            self.power_level = 8
            self.cook_time = '02:00'
            self.turn_table = True
        else:
            raise Error(f'please enter a valid cook program')
        print('program set to %s' % cook_program)

    def start_button(self):
        """run the microwave program"""
        self.check_power()
        if self.started:
            raise Error(f"program is running. Use stop button to stop it")
        else:
            self.started = True
            print('program %s started' % self.cook_program)
            self.set_timer(self.cook_time)
            self.started = False
            print('program(%s) ended' % self.cook_program)

    def stop_button(self):
        """stop the running program"""
        self.check_power()
        if not self.started:
            raise Error(f"no program is running. Use start button to start it")
        else:
            self.started = False
            print('program %s stopped' % self.cook_program)


if __name__ == "__main__":
    micr_obj = Microwave()
    micr_obj.power_button()
    micr_obj.set_power_level(3)
    micr_obj.cook_time = '01:00'
    micr_obj.start_button()
    micr_obj.set_program('popcorn')
    micr_obj.start_button()
